import time
import serial


# Serial port speed
serial_speed = 115200
# Serial device name (This one is for Bluetooth connection from mac)
serial_port = '/dev/cu.HC-05-DevB'


#Open serial over bluetooth
ser = serial.Serial(serial_port, serial_speed, timeout=5)

time.sleep(3)

#request temperature
ser.write("m")
print("sent: m")

data = ser.readline()
print(data + "*C")

#request lighting info
ser.write("l")
print("sent: l")

data = ser.readline()
print(data)

#request humidity
ser.write("h")
print("sent: h")

data = ser.readline()
print(data+"%")