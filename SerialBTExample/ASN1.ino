#include <SoftwareSerial.h>
#include <dht.h> 
dht DHT;
#define DHT22_PIN 7 
int temp=0;

int bluetoothTx = 2;
int bluetoothRx = 3;
int pin = 4;
int tempSend= 0;
long now = 0;
float tempe =0.0;
float humi=0.0;
SoftwareSerial bluetooth(bluetoothTx, bluetoothRx);


void setup()
{ 
Serial.begin(9600);
pinMode(pin, OUTPUT); 
digitalWrite(pin, LOW); 
bluetooth.begin(115200); 
bluetooth.print("$$$"); 
delay(100); 
bluetooth.println("U,9600,N"); 
bluetooth.begin(9600);
now = millis();
}

void loop() {


//read and update the temperature reading every 2 seconds.
//Using millis() instead of delay() as it will not freeze execution
if(millis()-now >=2000){
  int temp = DHT.read22(DHT22_PIN);
  tempe = DHT.temperature;
  humi = DHT.humidity;
  now = millis();
}

//A similar handling for the room lightning sensor should be created. 

//uncomment below for usage with usb serial connection
//char received = (char)Serial.read();


//read the bluetooth serial port and store into variable
char received = (char)bluetooth.read();

//Check for what was requested
//m stands for temperature measurement
if(received == 'm'){ 
  Serial.println(tempe); 
  bluetooth.print(tempe);}
//l stands for simulated value for lighting conditions
else if(received == 'l'){ 
  Serial.println("50%");
  bluetooth.print("50%");
} 
//and as a bonus, h stands for humidity 
else if(received == 'h'){
  Serial.println(humi);
  bluetooth.print(humi);
  }


}
